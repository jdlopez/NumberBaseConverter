package jdlopez.numberbaseconverter;

import android.content.ClipData;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.ClipboardManager;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    public static final int DECIMAL = 10;
    public static final int BINARY = 2;
    public static final int OCTAL = 8;
    public static final int HEXADECIMAL = 16;
    public static boolean fraction;
    public static String inputString = "";

    Spinner spinner;
    EditText userInput;
    Button submitBtn, decCopyBtn, binCopyBtn, octCopyBtn, hexCopyBtn;
    TextView decView, binView, octView, hexView;
    String baseNumber = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.base_number_spinner);

        userInput = findViewById(R.id.user_input);

        submitBtn = findViewById(R.id.submit_btn);
        decCopyBtn = findViewById(R.id.decimal_copy_btn);
        binCopyBtn = findViewById(R.id.binary_copy_btn);
        octCopyBtn = findViewById(R.id.octal_copy_btn);
        hexCopyBtn = findViewById(R.id.hexa_copy_btn);

        decView = findViewById(R.id.decimal_text_view_output);
        binView = findViewById(R.id.binary_text_view_output);
        octView = findViewById(R.id.octal_text_view_output);
        hexView = findViewById(R.id.hexa_text_view_output);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.base_numbers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                baseNumber = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                baseNumber = "Decimal";
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userInput.getText().toString().trim().equals(""))
                    userInput.setText("0");
                if (validNumber())
                    convertToDecBinOctHex();
                else
                    Toast.makeText(getApplicationContext(),"Please enter a valid number for that base",Toast.LENGTH_SHORT).show();
            }
        });

        decCopyBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vew){
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                String cp = decView.getText().toString();
                if(cp.equals(""))
                    cp = "0";
                ClipData clip = ClipData.newPlainText("Decimal value", cp);
                clipboard.setPrimaryClip(clip);
            }
        });

        binCopyBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vew){
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                String cp = binView.getText().toString();
                if(cp.equals(""))
                    cp = "0";
                ClipData clip = ClipData.newPlainText("Binary value", cp);
                clipboard.setPrimaryClip(clip);
            }
        });

        octCopyBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vew){
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                String cp = octView.getText().toString();
                if(cp.equals(""))
                    cp = "0";
                ClipData clip = ClipData.newPlainText("Octal value", cp);
                clipboard.setPrimaryClip(clip);
            }
        });

        hexCopyBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vew){
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                String cp = hexView.getText().toString();
                if(cp.equals(""))
                    cp = "0";
                ClipData clip = ClipData.newPlainText("Hexadecimal value", cp);
                clipboard.setPrimaryClip(clip);
            }
        });
    }

    /**
     * Checks if a decimal only has numbers,  binary only 1s and 0s, octal only 0-7, and hexadecimal only 0-F
     *
     * @return boolean
     */
    private boolean validNumber() {
        if (baseNumber.equals("Decimal"))
            return Pattern.matches("^(\\d*\\.)?\\d+$", userInput.getText().toString());
        if (baseNumber.equals("Binary"))
            return Pattern.matches("^([0-1]*\\.)?[0-1]+$", userInput.getText().toString());
        if (baseNumber.equals("Octal"))
            return Pattern.matches("^([0-7]*\\.)?[0-7]+$", userInput.getText().toString());
        if (baseNumber.equals("Hexadecimal"))
            return Pattern.matches("^([a-fA-F0-9]*\\.)?[a-fA-F0-9]+$", userInput.getText().toString());
        return false;
    }

    /**
     * Converts anything into decimal and then converts it to everything else(Binary, Octal, Hexa)
     */
    private void convertToDecBinOctHex() {
        inputString = userInput.getText().toString();
        isFraction();

        switch(baseNumber){
            case "Decimal":
                decView.setText(inputString);
                binView.setText(decToEverything(BINARY));
                octView.setText(decToEverything(OCTAL));
                hexView.setText(decToEverything(HEXADECIMAL));
                break;
            case "Binary":
                binView.setText(inputString);
                inputString = binOctToDec(BINARY);
                decView.setText(decToEverything(DECIMAL));
                octView.setText(decToEverything(OCTAL));
                hexView.setText(decToEverything(HEXADECIMAL));
                break;
            case "Octal":
                octView.setText(inputString);
                inputString = binOctToDec(OCTAL);
                decView.setText(decToEverything(DECIMAL));
                binView.setText(decToEverything(BINARY));
                hexView.setText(decToEverything(HEXADECIMAL));
                break;
            case "Hexadecimal":
                hexView.setText(inputString.toUpperCase());
                inputString = binOctToDec(HEXADECIMAL);
                decView.setText(decToEverything(DECIMAL));
                binView.setText(decToEverything(BINARY));
                octView.setText(decToEverything(OCTAL));
                break;
            default:

        }
    }

    /**
     * Converts bin/oct/hexa/ into decimal. If base is hexadecimal, then call hexFormat
     *
     * @param base the base converting from
     * @return The converted string
     */
    private String binOctToDec(int base) {
        double result = 0;

        double[] stringArray;

        int dotIndex;

        double tempResult = 0;

        dotIndex = inputString.indexOf(".");

        stringArray = stringToNumber(dotIndex, inputString.toCharArray(), base, base);

        tempResult = stringArray[1];
        result = stringArray[0];

        result += tempResult;
        String returnString = "" + result;

        returnString = fixThisStringPlease(returnString);

        return returnString;
    }

    /**
     * This is a helper method that converts a string into a decimal. I made this to also handle the case if the string contains any letters (A-F)
     *
     * @param dotIndex    Index of the dot
     * @param stringArray Input string in char array form
     * @param newBase     The base we are ultimately converting to. This is to check if we are converting to Hexa or not
     * @param toBase      Converts the string into the desired number. This is necessary for when we have a number of base 10 and want to convert to Hex
     * @return The string in number form
     */
    private double[] stringToNumber(int dotIndex, char[] stringArray, int newBase, int toBase) {
        double tempResult = 0;
        int result = 0;

        if(newBase == toBase){

        }
        if (fraction) {
            for (int i = -1, j = dotIndex + 1; j < stringArray.length; i--, j++) {
                int x;
                if (newBase == HEXADECIMAL)
                    x = hexFormat(stringArray[j] + "");
                else
                    x = Integer.parseInt(stringArray[j] + "");

                double temp = (Math.pow(toBase, i));

                tempResult += round(x * temp, (-1) * i);
//                    tempResult += x * temp;
            }
        }

        if (!fraction)
             dotIndex = stringArray.length;

        for (int i = dotIndex - 1, j = 0; i >= 0; i--, j++) {
            int x;

            if (newBase == HEXADECIMAL)
                x = hexFormat(stringArray[j] + "");
            else
                x = Integer.parseInt(stringArray[j] + "");

            result += x * Math.pow(toBase, i);
        }

        double[] num = {result, tempResult};

        return num;
    }

    /**
     * Converts decimal into anything(Binary, Octal, Hexadecimal)
     *
     * @param newBase The base to convert to
     * @return Newly converted string
     */
    private String decToEverything(int newBase) {
        String newString = "";
        int result;
        double[] stringArray;
        int dotIndex;
        double tempResult;

        dotIndex = inputString.indexOf(".");


        if (!fraction && Integer.parseInt(inputString) == 0)
            return "0";

        /*
         *  separate into whole and fractional part
         */

        stringArray = stringToNumber(dotIndex, inputString.toCharArray(), newBase, DECIMAL);

        tempResult = stringArray[1];
        result = (int) stringArray[0];

        ArrayList<Integer> wholeNumArray = new ArrayList<>();
        ArrayList<Integer> fractionNumArray = new ArrayList<>();

        /*
         *  Convert whole number into the new base
         */
        while (result != 0) {
            int tempWholeNum = result % newBase;
            wholeNumArray.add(tempWholeNum);

            result /= newBase;
        }

        /*
         *  Convert fraction number into the new base
         */
        while (tempResult != 0.0) {
            double fractionNamConversation = tempResult * newBase;
            int tempFractionNum = (int) fractionNamConversation;
            fractionNumArray.add(tempFractionNum);

            tempResult = fractionNamConversation - tempFractionNum;
        }

        // Feed wholeNumArray into newString backwards and formatting if its hexa
        for (int i = wholeNumArray.size() - 1; i >= 0; i--) {
            int numToAdd = wholeNumArray.get(i);

            if (newBase == HEXADECIMAL)
                newString += hexFormat(numToAdd);
            else
                newString += numToAdd;
        }

        // Feed fractionNumArray into newString and formatting if its hexa
        if (fraction)
            newString += ".";
        for (int i = 0; i <= fractionNumArray.size() - 1; i++) {
            int numToAdd = fractionNumArray.get(i);

            if (newBase == HEXADECIMAL)
                newString += hexFormat(numToAdd);
            else
                newString += numToAdd;
        }

        newString = fixThisStringPlease(newString);

        return round(newString, 8);
    }

    /**
     * Changes hexFormat from letter to int
     *
     * @param letter letter to change into int
     * @return new int
     */
    private int hexFormat(String letter) {
        switch (letter.toUpperCase()) {
            case "A":
                return 10;
            case "B":
                return 11;
            case "C":
                return 12;
            case "D":
                return 13;
            case "E":
                return 14;
            case "F":
                return 15;
            default:
                return Integer.parseInt(letter);
        }
    }

    /**
     * Changes hexFormate from int to letter
     *
     * @param num int to change into letter
     * @return new letter
     */
    private String hexFormat(int num) {
        switch (num) {
            case 10:
                return "A";
            case 11:
                return "B";
            case 12:
                return "C";
            case 13:
                return "D";
            case 14:
                return "E";
            case 15:
                return "F";
            default:
                return "" + num;
        }
    }

    /**
     * Checks if there is a dot in the inputString, truncates any 0's in front and if there is an a dot with nothing after
     */
    private void isFraction() {
        if (inputString.contains(".")) {
            fraction = true;
            inputString = fixThisStringPlease(inputString);

            return;
        }
        fraction = false;
    }

    /**
     * Adds a zero in front of the dot if there is none
     *
     * @param fixedString
     * @return string with 0 in front of dot
     */
    private String addZeroIfStartingDot(String fixedString) {
        if (fixedString.startsWith(".")) {
            fixedString = 0 + fixedString;
        }
        return fixedString;
    }

    /**
     * Checks if the string ends with a dot, if so then delete it
     *
     * @param fixedString
     * @return string without dot at the end
     */
    private String deleteTerminatingDot(String fixedString) {
        fixedString = fixedString.indexOf(".") < 0 ? fixedString : fixedString.replaceAll("0*$", "").replaceAll("\\.$", "");
        return fixedString;
    }

    /**
     * Calls addZeroIfStartingDot and deleteTerminatingDot to fix anything wrong with the string
     *
     * @param badString
     * @return fixed string
     */
    private String fixThisStringPlease(String badString) {

        badString = addZeroIfStartingDot(badString);
        badString = deleteTerminatingDot(badString);
        return badString;
    }

    /**
     * This rounds a double to n decimal places
     *
     * @param value  The number to round
     * @param places The number of decimal places to round to
     * @return Rounded double
     */
    private static String round(String value, int places){
        int dotIndex = value.indexOf(".");
        int decimalPlacement = value.length() - dotIndex;

        if(!fraction || decimalPlacement < places)
            return value;

        String result = value.substring(0, dotIndex + places + 1);  // + 1 to move over from the dot index

        return result;
    }

    /**
     * This rounds a double to n decimal places
     *
     * @param value  The number to round
     * @param places The number of decimal places to round to
     * @return Rounded double
     */
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
